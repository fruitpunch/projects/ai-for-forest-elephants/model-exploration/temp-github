# This step creates the actual clips and put them in a datafile. 

import boto3
import pandas as pd
from src.data.preprocess import preprocess_training_df, get_clips_from_file, add_random_clips
from tqdm import tqdm
import ast
import os
from collections import defaultdict
from scipy.io import wavfile

client = boto3.client(
    service_name='s3',
    region_name='us-east-2',
    aws_access_key_id='AKIAR2Q47EXTL23Q7RK4',
    aws_secret_access_key='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'
)

s3 = boto3.resource(
    service_name='s3',
    region_name='us-east-2',
    aws_access_key_id='AKIAR2Q47EXTL23Q7RK4',
    aws_secret_access_key='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'
)


training_df = pd.read_csv('./data/training_data_with_random_clips.csv',)

col_with_lists = [
    'Rumble Begin Time (s)',
       'Rumble End Time (s)', 'Rumble File Offset (s)', 'Rumble Duration (s)',
       'Gunshot Begin Time (s)', 'Gunshot End Time (s)',
       'Gunshot File Offset (s)', 'Gunshot Duration (s)',
        'Other File Offset (s)', 'Other Duration (s)',
]


for col in col_with_lists:
    training_df[col] = training_df[col].apply(ast.literal_eval)

  

classes = [
 'Rumble', 'Gunshot', 'Other',
]

class ClipManager:
    
    def __init__(self, base_location='./data/training', classes=classes):
        self.base_location = base_location
        self.classes = classes
        
        
        self.map_counter = defaultdict(lambda: 0)
        
        self.current_dir = {c: 0 for c in classes}
    
    def get_loc(self, clip):
        
        target_dir = self.current_dir[clip.sound_class]
        nth_file = self.map_counter[(clip.sound_class, target_dir)]
        
        # Update counter
        self.map_counter[(clip.sound_class, target_dir)] += 1
        
        if self.map_counter[(clip.sound_class, target_dir)] > 5000:
            self.current_dir[clip.sound_class] += 1
        dir_loc = f'{self.base_location}/{clip.sound_class}/{target_dir}'
        save_loc =  f'{dir_loc}/{nth_file}.wav'
        return dir_loc, save_loc


clip_manager = ClipManager()

for i, row in tqdm(training_df.iterrows(), total=len(training_df.index)):

    clips = get_clips_from_file(s3, row, classes)
    
    for j, clip in enumerate(clips):
        loc_dir, save_location = clip_manager.get_loc(clip)
        os.makedirs(loc_dir, exist_ok=True, )
       
        wavfile.write(save_location, clip.sample_rate, clip.data)
     