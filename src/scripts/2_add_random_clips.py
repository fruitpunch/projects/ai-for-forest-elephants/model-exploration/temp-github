# Adds random clips to the merged dataframes from the previous step

import boto3
import pandas as pd
from src.data.preprocess import preprocess_training_df, get_clips_from_file, add_random_clips
from tqdm import tqdm
import ast

client = boto3.client(
    service_name='s3',
    region_name='us-east-2',
    aws_access_key_id='AKIAR2Q47EXTL23Q7RK4',
    aws_secret_access_key='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'
)

s3 = boto3.resource(
    service_name='s3',
    region_name='us-east-2',
    aws_access_key_id='AKIAR2Q47EXTL23Q7RK4',
    aws_secret_access_key='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'
)


training_df = pd.read_csv('./data/training_info.csv',)

print(training_df.columns)

col_with_lists = [
    'Rumble Begin Time (s)',
       'Rumble End Time (s)', 'Rumble File Offset (s)', 'Rumble Duration (s)',
       'Gunshot Begin Time (s)', 'Gunshot End Time (s)',
       'Gunshot File Offset (s)', 'Gunshot Duration (s)'
]

def to_lists(x):
    if type(x) == str:
        return ast.literal_eval(x)
    else:
        return []
    
    

for col in col_with_lists:
    training_df[col] = training_df[col].apply(to_lists)


# Here we add random clips

training_df = add_random_clips(training_df)

print(training_df.head())
print(training_df.columns)

training_df.to_csv('./data/training_data_with_random_clips.csv',index=False)

