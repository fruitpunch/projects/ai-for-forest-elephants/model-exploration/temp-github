# Creates a merged dataframe containing information about the audio files and when there are rumbles

import boto3
import pandas as pd
from src.data.load import get_buckets, load_rumble_clip_data, load_gunshot_clip_data
from src.data.preprocess import preprocess_training_df, get_clips_from_file, merge_rumble_and_gunshots
from tqdm import tqdm

# Load the client and the resources
client = boto3.client(
    service_name='s3',
    region_name='us-east-2',
    aws_access_key_id='AKIAR2Q47EXTL23Q7RK4',
    aws_secret_access_key='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'
)

s3 = boto3.resource(
    service_name='s3',
    region_name='us-east-2',
    aws_access_key_id='AKIAR2Q47EXTL23Q7RK4',
    aws_secret_access_key='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'
)

# Get the buckets from which we want to read the files
s3_bucket = s3.Bucket('data-ai-for-forest-elephants')
text_buckets, wav_buckets = get_buckets(s3_bucket)


gunshot_df = load_gunshot_clip_data(s3_bucket)
rumble_df = load_rumble_clip_data(s3_bucket)


preprocessed_gunshot_df =  preprocess_training_df(gunshot_df, wav_buckets)
preprocessed_rumble_df =  preprocess_training_df(rumble_df, wav_buckets)

training_df = merge_rumble_and_gunshots(preprocessed_rumble_df, preprocessed_gunshot_df)


training_df.to_csv('./data/training_info.csv', index=False)








